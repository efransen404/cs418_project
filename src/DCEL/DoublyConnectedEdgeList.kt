package DCEL

import data.LineSegment
import data.Point

class Vertex(val name: String, val x: Double, val y: Double) {
    var incidentEdge: HalfEdge? = null
    lateinit var incidentEdgeString: String
}

class Face(val name: String, var innerStrings: List<String>, var outerString: String?) {
    var outerComponent: HalfEdge? = null
    var innerComponents: MutableList<HalfEdge> = mutableListOf()
}

class HalfEdge(val name: String) {
    var origin: Vertex? = null
    var twin: HalfEdge? = null
    var incidentFace: Face? = null
    var next: HalfEdge? = null
    var prev: HalfEdge? = null

    lateinit var originString: String
    lateinit var twinString: String
    lateinit var incidentFaceString: String
    lateinit var nextString: String
    lateinit var prevString: String
}

class DoublyConnectedEdgeList (source: String) {
    val vertices: MutableMap<String, Vertex> = HashMap()
    val faces: MutableMap<String, Face> = HashMap()
    val edges: MutableMap<String, HalfEdge> = HashMap()

    init {
        val lines = source.split("\n")
        val vertexEdgeStrings: MutableMap<String, Vertex> = HashMap()
        lines.forEach {line ->
            if(line.isNotEmpty()) {
                when(line[0]) {
                    'v' -> {
                        val v = parseVertex(line)
                        vertices[v.name] = v
                        vertexEdgeStrings[v.incidentEdgeString] = v
                    }
                    'f' -> {
                        val f = parseFace(line)
                        faces[f.name] = f
                    }
                    'e' -> {
                        val e = parseEdge(line)
                        edges[e.name] = e
                    }
                    else -> {
                        throw Exception()
                    }
                }
            }
        }
        // Assign the incident edges
        vertices.forEach { pair ->
            val vertex = pair.value
            vertex.incidentEdge = edges[vertex.incidentEdgeString]
        }
        // Assign the face components
        faces.forEach {pair ->
            val face = pair.value
            val innerStrings = face.innerStrings
            val outerString = face.outerString
            if(innerStrings.isNotEmpty()) {
                innerStrings.forEach {
                    face.innerComponents.add(edges[it]!!)
                }
            }
            if(outerString != null) {
                face.outerComponent = edges[outerString]
            }
        }
        // Assign the edges
        edges.forEach {pair ->
            val edge = pair.value
            edge.origin = vertices[edge.originString]
            edge.twin = edges[edge.twinString]
            edge.incidentFace = faces[edge.incidentFaceString]
            edge.next = edges[edge.nextString]
            edge.prev = edges[edge.prevString]
        }
    }
    private fun parseVertex(input: String): Vertex {
        val tokens = input.split("  ")
        val name = tokens[0]
        val pair = tokens[1].replace("(","").replace(")","").replace(",","")
        val splitPair = pair.split(" ")
        val x = splitPair[0].toDouble()
        val y = splitPair[1].toDouble()
        val edgeString = tokens[2]
        val vertex = Vertex(name, x, y)
        vertex.incidentEdgeString = edgeString
        return vertex
    }
    private fun parseFace(input: String): Face {
        val tokens = input.split("  ")
        val name = tokens[0]
        val outer = if(tokens[1] == "nil") null else tokens[1]
        val inner = if(tokens[2] == "nil") null else tokens[2]
        val innerList = inner?.split(";") ?: listOf()
        return Face(name, innerList, outer)
    }
    private fun parseEdge(input: String): HalfEdge {
        val tokens = input.split("  ")
        val name = tokens[0]
        val origin = tokens[1]
        val twin = tokens[2]
        val incidentFace = tokens[3]
        val next = tokens[4]
        val prev = tokens[5]
        val edge = HalfEdge(name)
        edge.originString = origin
        edge.twinString = twin
        edge.incidentFaceString = incidentFace
        edge.nextString = next
        edge.prevString = prev
        return edge
    }

    fun getEdges(): List<LineSegment> {
        val result: MutableList<LineSegment> = ArrayList()
        val visited: MutableSet<String> = HashSet()
        val faces = faces.values
        faces.forEach {face ->
            val innerComponents = face.innerComponents
            for(i in 0 until innerComponents.size) {
                val start = innerComponents[i]
                var e = start
                while(e.next != start) {
                    // Get all but the last edge
                    if(!visited.contains(e.name) && !visited.contains(e.twin!!.name)) {
                        val p = e.origin!!
                        val q = e.next!!.origin!!
                        result += LineSegment(Point(p.x, p.y), Point(q.x, q.y), face.name)
                        visited.add(e.name)
                    }
                    e = e.next!!
                }
                // Gets the last edge
                if(!visited.contains(e.name) && !visited.contains(e.twin!!.name)) {
                    val p = e.origin!!
                    val q = e.next!!.origin!!
                    result += LineSegment(Point(p.x, p.y), Point(q.x, q.y), face.name)
                    visited.add(e.name)
                }
            }
            val outerComponent = face.outerComponent
            if(outerComponent != null) {
                val start:HalfEdge = outerComponent
                var e = start
                // Get all but the last edge
                while(e.next != start) {
                    if(!visited.contains(e.name) && !visited.contains(e.twin!!.name)) {
                        val p = e.origin!!
                        val q = e.next!!.origin!!
                        result += LineSegment(Point(p.x, p.y), Point(q.x, q.y), face.name)
                        visited.add(e.name)
                    }
                    e = e.next!!
                }
                // Gets the last edge
                if(!visited.contains(e.name) && !visited.contains(e.twin!!.name)) {
                    val p = e.origin!!
                    val q = e.next!!.origin!!
                    result += LineSegment(Point(p.x, p.y), Point(q.x, q.y), face.name)
                    visited.add(e.name)
                }
            }
        }
        return result
    }
}