package trapezoidMap

import data.LineSegment
import data.OutOfBoundsException
import data.Point
import kotlin.random.Random

interface TrapezoidGraphNode {
    var parents: MutableList<TrapezoidGraphNode>
    var left: TrapezoidGraphNode
    var right: TrapezoidGraphNode
}

class TrapezoidGraphXNode(var point: Point, override var parents: MutableList<TrapezoidGraphNode>): TrapezoidGraphNode {
    override lateinit var left: TrapezoidGraphNode
    override lateinit var right: TrapezoidGraphNode
    override fun toString(): String {
        return point.toString()
    }
}

class TrapezoidGraphYNode(var segment: LineSegment, override var parents: MutableList<TrapezoidGraphNode>): TrapezoidGraphNode {
    override lateinit var left: TrapezoidGraphNode
    override lateinit var right: TrapezoidGraphNode
    override fun toString(): String {
        return segment.toString()
    }
}

class TrapezoidGraphLeaf(var trapezoid: Trapezoid, override var parents: MutableList<TrapezoidGraphNode>): TrapezoidGraphNode {
    override lateinit var left: TrapezoidGraphNode
    override lateinit var right: TrapezoidGraphNode
    init {
        trapezoid.leaf = this
    }
    override fun toString(): String {
        return trapezoid.toString()
    }
}

class TrapezoidMap(segments: List<LineSegment>) {
    private var root: TrapezoidGraphNode? = null

    init {
        var minX: Double? = null
        var maxX: Double? = null
        var minY: Double? = null
        var maxY: Double? = null
        for(i in 0 until segments.size) {
            val line = segments[i]
            if(minX == null || minX > line.start.x) minX = line.start.x
            if(maxX == null || maxX < line.end.x) maxX = line.end.x
            if(minY == null || minY > line.start.y) minY = line.start.y
            if(minY > line.end.y) minY = line.end.y
            if(maxY == null || maxY < line.start.y) maxY = line.start.y
            if(maxY < line.end.y) maxY = line.end.y
        }
        if(minX != null || maxX != null || minY != null || maxY != null) {
            val top = LineSegment(Point(minX!!, maxY!!), Point(maxX!!, maxY), "T")
            val bottom = LineSegment(Point(minX, minY!!), Point(maxX, minY), "B")
            val leftP = Point(minX, minY)
            val rightP = Point(maxX, maxY)
            val tR = Trapezoid(
                top,
                bottom,
                leftP,
                rightP,
                null,
                null,
                null,
                null
            )
            root = TrapezoidGraphLeaf(tR, mutableListOf())
        }
        val randomList = randomPermutation(segments)
        randomList.forEach {
            addSegment(it)
        }
    }

    fun getAllTrapezoids(): List<Trapezoid> {
        val root = root ?: return listOf()
        return getAllTrapezoids(root).toList()
    }

    fun getAllTrapezoids(node: TrapezoidGraphNode): MutableSet<Trapezoid> {
        return if(node is TrapezoidGraphLeaf) {
            val set = HashSet<Trapezoid>()
            set += node.trapezoid
            set
        } else {
            val set = getAllTrapezoids(node.left)
            set.addAll(getAllTrapezoids(node.right))
            set
        }
    }

    fun toFileString(): String {
        val trapezoids = getAllTrapezoids()
        val faceList: HashMap<String, MutableList<Trapezoid>> = HashMap()
        trapezoids.forEach {t ->
            val list = faceList[t.top.face]
            if(list == null) {
                faceList[t.top.face] = ArrayList()
                faceList[t.top.face]!!.add(t)
            } else {
                list.add(t)
            }
        }
        var result = "********** Trapezoidal Map **********\n\n"
        faceList.forEach {pair ->
            val face = pair.key
            val list = pair.value
            result += "Face $face contains " + list.size + " trapezoids:\n\n"
            list.forEach {t ->
                result += t.toString()
            }
        }

        return result
    }

    fun <T> randomPermutation (original: List<T>): List<T> {
        val newList = original.toMutableList()
        val n = original.size - 1
        for(k in n downTo 2) {
            val rndIndex = Random.nextInt(0,k)
            newList[k] = original[rndIndex]
            newList[rndIndex] = original[k]
        }
        return newList
    }

    fun followSegment(s: LineSegment): List<Trapezoid> {
        val result: MutableList<Trapezoid> = ArrayList()
        val p = s.start
        val q = s.end
        var t: Trapezoid = locate(s)?.trapezoid ?: return result
        result += t
        while(q.compareX(t.rightP) == Point.RIGHT) {
            result += t
            val lowerRight = t.tRight
            val upperRight = t.tRight?.tAbove
            try {
                if (s.findSide(p) == LineSegment.ABOVE) {
                    if(lowerRight != null)
                        t = lowerRight
                    else
                        break
                } else {
                    if(upperRight != null)
                        t = upperRight
                    else
                        if(lowerRight != null)
                            t = lowerRight
                        else
                            break
                }
            } catch (e: OutOfBoundsException) {
                return result
            }
        }
        return result
    }

    fun addSegment(s: LineSegment) {
        val intersected = followSegment(s)
        val size = intersected.size
        if(size == 1) {
            addSegment(s, intersected[0].leaf)
        } else {
            val nodes: MutableList<TrapezoidGraphYNode> = ArrayList()
            if(intersected.isEmpty()) return
            // trapezoid 0
            if(intersected[0].leftP == s.start) {
                nodes.add(addSegment_split(s, intersected[0].leaf))
            } else {
                nodes.add(addSegment_internalStart(s, intersected[0].leaf))
            }
            // go from 1 to k - 1
            for(i in 1..size - 2) {
                nodes.add(addSegment_split(s, intersected[i].leaf))
            }
            // trapezoid k
            if(intersected[size - 1].rightP == s.end) {
                nodes.add(addSegment_split(s, intersected[size - 1].leaf))
            } else {
                nodes.add(addSegment_internalEnd(s, intersected[size - 1].leaf))
            }
        }
    }



    /**
     * Functions for adding a segment across multiple trapezoids
     */
    fun addSegment_split(s: LineSegment, node: TrapezoidGraphLeaf): TrapezoidGraphYNode {
        // grab the original trapezoid and it's data
        val t = node.trapezoid
        val top = t.top
        val bottom = t.bottom
        val leftP = t.leftP
        val rightP = t.rightP
        val tAbove = t.tAbove
        val tBelow = t.tBelow
        val tLeft = t.tLeft
        val tRight = t.tRight

        // Top trapezoid
        val tA = Trapezoid(
            top,
            s,
            leftP,
            rightP,
            tAbove,
            null,
            tLeft,
            tRight)
        // Bottom trapezoid
        val tB = Trapezoid(
            s,
            bottom,
            leftP,
            rightP,
            tA,
            tBelow,
            tLeft,
            tRight)
        // add missing connection
        tA.tBelow = tB
        // Add surrounding connections
        if(tLeft != null && tLeft.tRight == t) tLeft.tRight = tB
        if(tRight != null && tRight.tLeft == t) tRight.tLeft = tB
        if(tAbove != null && tAbove.tBelow == t) tAbove.tBelow = tA
        if(tBelow != null && tBelow.tAbove == t) tBelow.tAbove = tB

        // create the tree
        val tempNode = TrapezoidGraphYNode(s, node.parents)
        tempNode.left = TrapezoidGraphLeaf(tA, mutableListOf(tempNode))
        tempNode.right = TrapezoidGraphLeaf(tB, mutableListOf(tempNode))

        replaceNode(node, tempNode)
        return tempNode
    }

    fun addSegment_internalStart(s: LineSegment, node: TrapezoidGraphLeaf): TrapezoidGraphYNode {
        // grab the original trapezoid and it's data
        val t = node.trapezoid
        val top = t.top
        val bottom = t.bottom
        val leftP = t.leftP
        val rightP = t.rightP
        val tAbove = t.tAbove
        val tBelow = t.tBelow
        val tLeft = t.tLeft
        val tRight = t.tRight

        // Left trapezoid
        val tA = Trapezoid(
            top,
            bottom,
            leftP,
            s.start,
            tAbove,
            tBelow,
            tLeft,
            null)
        // Top right trapezoid
        val tB = Trapezoid(
            top,
            s,
            s.start,
            rightP,
            tAbove,
            null,
            tA,
            tRight)
        // Lower right trapezoid
        val tC = Trapezoid(
            s,
            bottom,
            s.start,
            rightP,
            tB,
            tBelow,
            tA,
            tRight)
        // Add a couple missing connections
        tA.tRight = tC
        tB.tBelow = tC

        // Add surrounding connections
        if(tLeft != null && tLeft.tRight == t) tLeft.tRight = tA
        if(tRight != null && tRight.tLeft == t) tRight.tLeft = tC
        if(tAbove != null && tAbove.tBelow == t) tAbove.tBelow = tA
        if(tBelow != null && tBelow.tAbove == t) tBelow.tAbove = tA

        // create the tree
        val newNode = TrapezoidGraphXNode(s.start, node.parents)
        newNode.left = TrapezoidGraphLeaf(tA, mutableListOf(newNode))
        val tempNode = TrapezoidGraphYNode(s, mutableListOf(newNode))
        newNode.right = tempNode
        tempNode.left = TrapezoidGraphLeaf(tB, mutableListOf(tempNode))
        tempNode.right = TrapezoidGraphLeaf(tC, mutableListOf(tempNode))

        replaceNode(node, newNode)
        return tempNode
    }

    fun addSegment_internalEnd(s: LineSegment, node: TrapezoidGraphLeaf): TrapezoidGraphYNode {
        // grab the original trapezoid and it's data
        val t = node.trapezoid
        val top = t.top
        val bottom = t.bottom
        val leftP = t.leftP
        val rightP = t.rightP
        val tAbove = t.tAbove
        val tBelow = t.tBelow
        val tLeft = t.tLeft
        val tRight = t.tRight

        // right trapezoid
        val tA = Trapezoid(
            top,
            bottom,
            s.end,
            rightP,
            tAbove,
            tBelow,
            null,
            tRight)
        // top left trapezoid
        val tB = Trapezoid(
            top,
            s,
            leftP,
            s.end,
            tAbove,
            null,
            tLeft,
            tA)
        // bottom left trapezoid
        val tC = Trapezoid(
            s,
            bottom,
            leftP,
            s.end,
            tB,
            tBelow,
            tLeft,
            tA)
        tA.tLeft = tC
        tB.tBelow = tC

        if(tLeft?.tRight == t) tLeft.tRight = tC
        if(tRight?.tLeft == t) tRight.tLeft = tA
        if(tAbove?.tBelow == t) tAbove.tBelow = tB
        if(tBelow?.tAbove == t) tBelow.tAbove = tC

        // create the tree
        val newNode = TrapezoidGraphXNode(s.end, node.parents)
        newNode.right = TrapezoidGraphLeaf(tA, mutableListOf(newNode))
        val tempNode = TrapezoidGraphYNode(s, mutableListOf(newNode))
        newNode.left = tempNode
        tempNode.left = TrapezoidGraphLeaf(tB, mutableListOf(tempNode))
        tempNode.right = TrapezoidGraphLeaf(tC, mutableListOf(tempNode))

        replaceNode(node, newNode)
        return tempNode
    }

    /**
     * Functions for adding a segment contained inside one trapezoid
     */

    fun addSegment(s: LineSegment, node: TrapezoidGraphLeaf) {
        val t = node.trapezoid
        if(t.leftP == s.start) {
            if(t.rightP == s.end) {
                addSegment_commonBoth(s, node)
            } else {
                addSegment_commonLeft(s, node)
            }
        } else {
            if(t.rightP == s.end) {
                addSegment_commonRight(s, node)
            } else {
                addSegment_commonNone(s, node)
            }
        }
    }

    fun addSegment_commonBoth(s: LineSegment, node: TrapezoidGraphLeaf): TrapezoidGraphYNode {
        // grab the original trapezoid and it's data
        val t = node.trapezoid
        val top = t.top
        val bottom = t.bottom
        val leftP = t.leftP
        val rightP = t.rightP
        val tAbove = t.tAbove
        val tBelow = t.tBelow
        val tLeft = t.tLeft
        val tRight = t.tRight

        // create the top trapezoid
        val tA = Trapezoid(
            top,
            s,
            leftP,
            rightP,
            tAbove,
            null,
            tLeft,
            tRight)
        // create the bottom trapezoid
        val tB = Trapezoid(
            s,
            bottom,
            leftP,
            rightP,
            tA,
            tBelow,
            tLeft,
            tRight)
        tA.tBelow = tB

        // connect the surrounding trapezoids
        if(tAbove != null && tAbove.tBelow == t) tAbove.tBelow = tA
        if(tBelow != null && tBelow.tAbove == t) tBelow.tAbove = tB
        if(tLeft != null && tLeft.tRight == t) tLeft.tRight = tB
        if(tRight != null && tRight.tLeft == t) tRight.tLeft = tB

        // create a tree
        val tempNode = TrapezoidGraphYNode(s, node.parents)
        tempNode.left = TrapezoidGraphLeaf(tA, mutableListOf(tempNode))
        tempNode.right = TrapezoidGraphLeaf(tB, mutableListOf(tempNode))
        replaceNode(node, tempNode)
        return tempNode
    }

    fun addSegment_commonLeft(s: LineSegment, node: TrapezoidGraphLeaf): TrapezoidGraphXNode {
        // grab the original trapezoid and it's data
        val t = node.trapezoid
        val top = t.top
        val bottom = t.bottom
        val leftP = t.leftP
        val rightP = t.rightP
        val tAbove = t.tAbove
        val tBelow = t.tBelow
        val tLeft = t.tLeft
        val tRight = t.tRight

        // Create the trapezoid on the right
        val tA = Trapezoid(
            top,
            bottom,
            s.end,
            rightP,
            tAbove,
            tBelow,
            null,
            tRight)
        // Create the trapezoid on the top left
        val tB = Trapezoid(
            top,
            s,
            leftP,
            s.end,
            tAbove,
            null,
            tLeft,
            tA)
        // Create the trapezoid on the lower left
        val tC = Trapezoid(
            s,
            bottom,
            leftP,
            s.end,
            tB,
            tBelow,
            tLeft,
            tA)
        // Link up a couple missing connections
        tA.tLeft = tC
        tB.tBelow = tC

        // Link up the surrounding trapezoids
        if(tLeft != null && tLeft.tRight == t) tLeft.tRight = tC
        if(tRight != null && tRight.tLeft == t) tRight.tLeft = tA
        if(tAbove != null && tAbove.tBelow == t) tAbove.tBelow = tB
        if(tBelow != null && tBelow.tAbove == t) tBelow.tAbove = tC

        // create a tree
        val tempNode = TrapezoidGraphXNode(s.end, node.parents)
        tempNode.right = TrapezoidGraphLeaf(tA, mutableListOf(tempNode))
        tempNode.left = TrapezoidGraphYNode(s, mutableListOf(tempNode))
        tempNode.left.left = TrapezoidGraphLeaf(tB, mutableListOf(tempNode))
        tempNode.left.right = TrapezoidGraphLeaf(tC, mutableListOf(tempNode))

        // add the tree to the graph
        replaceNode(node, tempNode)
        return tempNode
    }

    fun addSegment_commonRight(s: LineSegment, node: TrapezoidGraphLeaf): TrapezoidGraphXNode {
        // grab the original trapezoid and it's data
        val t = node.trapezoid
        val top = t.top
        val bottom = t.bottom
        val leftP = t.leftP
        val rightP = t.rightP
        val tAbove = t.tAbove
        val tBelow = t.tBelow
        val tLeft = t.tLeft
        val tRight = t.tRight

        // create the trapezoid on the left
        val tA = Trapezoid(
            top,
            bottom,
            leftP,
            s.start,
            tAbove,
            tBelow,
            tLeft,
            null)
        // create the trapezoid on the upper right
        val tB = Trapezoid(
            top,
            s,
            s.start,
            rightP,
            tAbove,
            null,
            tA,
            tRight)
        // create the trapezoid on the lower right
        val tC = Trapezoid(
            s,
            bottom,
            s.start,
            rightP,
            tB,
            tBelow,
            tA,
            tRight)
        // Add a couple missing connections
        tA.tRight = tC
        tB.tBelow = tC

        // Add the surrounding connections
        if(tLeft != null && tLeft.tRight == t) tLeft.tRight = tA
        if(tRight != null && tRight.tLeft == t) tRight.tLeft = tC
        if(tAbove != null && tAbove.tBelow == t) tAbove.tBelow = tA
        if(tBelow != null && tBelow.tAbove == t) tBelow.tAbove = tA

        // create a tree
        val tempNode = TrapezoidGraphXNode(s.start, node.parents)
        tempNode.left = TrapezoidGraphLeaf(tA, mutableListOf(tempNode))
        tempNode.right = TrapezoidGraphYNode(s, mutableListOf(tempNode))
        tempNode.right.left = TrapezoidGraphLeaf(tB, mutableListOf(tempNode.right))
        tempNode.right.right = TrapezoidGraphLeaf(tC, mutableListOf(tempNode.right))

        replaceNode(node, tempNode)
        return tempNode
    }

    fun addSegment_commonNone(s: LineSegment, node: TrapezoidGraphLeaf): TrapezoidGraphXNode {
        // grab the original trapezoid and it's data
        val t = node.trapezoid
        val top = t.top
        val bottom = t.bottom
        val leftP = t.leftP
        val rightP = t.rightP
        val tAbove = t.tAbove
        val tBelow = t.tBelow
        val tLeft = t.tLeft
        val tRight = t.tRight

        // The left trapezoid
        val tA = Trapezoid(
            top,
            bottom,
            leftP,
            s.start,
            tAbove,
            tBelow,
            tLeft,
            null)
        // The right trapezoid
        val tB = Trapezoid(
            top,
            bottom,
            s.end,
            rightP,
            tAbove,
            tBelow,
            tRight,
            null)
        // The center top trapezoid
        val tC = Trapezoid(
            top,
            s,
            s.start,
            s.end,
            tAbove,
            null,
            tA,
            tB)
        // The center bottom trapezoid
        val tD = Trapezoid(
            s,
            bottom,
            s.start,
            s.end,
            tC,
            tBelow,
            tA,
            tB)
        // Add missing connections
        tA.tRight = tD
        tB.tLeft = tD
        tC.tBelow = tD

        // Add surrounding connections
        if(tLeft != null && tLeft.tRight == t) tLeft.tRight = tA
        if(tRight != null && tRight.tLeft == t) tRight.tLeft = tB
        if(tAbove != null && tAbove.tBelow == t) tAbove.tBelow = tA
        if(tBelow != null && tBelow.tAbove == t) tBelow.tAbove = tA

        // create a tree
        val tempNode = TrapezoidGraphXNode(s.start, node.parents)
        tempNode.left = TrapezoidGraphLeaf(tA, mutableListOf(tempNode))
        tempNode.right = TrapezoidGraphXNode(s.end, mutableListOf(tempNode))
        tempNode.right.right = TrapezoidGraphLeaf(tB, mutableListOf(tempNode.right))
        tempNode.right.left = TrapezoidGraphYNode(s, mutableListOf(tempNode.right))
        tempNode.right.left.left = TrapezoidGraphLeaf(tC, mutableListOf(tempNode.right.left))
        tempNode.right.left.right = TrapezoidGraphLeaf(tD, mutableListOf(tempNode.right.left))

        replaceNode(node, tempNode)
        return tempNode
    }

    fun replaceNode(old: TrapezoidGraphNode, new: TrapezoidGraphNode) {
        if(old.parents.isEmpty()) {
            // old is the root
            root = new
        } else {
            old.parents.forEach {parent ->
                if(parent.left == old) parent.left = new
                else parent.right = new
            }
        }
    }

    private fun locate(s: LineSegment): TrapezoidGraphLeaf? {
        val root = root ?: return null
        return locate(s.start, s, root)
    }

    private fun locate(p: Point, s: LineSegment, node: TrapezoidGraphNode): TrapezoidGraphLeaf {
        when (node) {
            is TrapezoidGraphXNode -> {
                return when(p.compareX(node.point)) {
                    Point.SIDE.LEFT -> locate(p, s, node.left)
                    Point.SIDE.ON -> locate(p, s, node.right)
                    Point.SIDE.RIGHT -> locate(p, s, node.right)
                }
            }
            is TrapezoidGraphYNode -> {
                return when(node.segment.findSide(p)) {
                    LineSegment.SIDE.ABOVE -> locate(p, s, node.left)
                    LineSegment.SIDE.BELOW -> locate(p, s, node.right)
                    LineSegment.SIDE.ON -> {
                        return if(s.slope < node.segment.slope) {
                            locate(p, s, node.left)
                        } else {
                            locate(p, s, node.right)
                        }
                    }
                }
            }
            is TrapezoidGraphLeaf -> {
                return node
            }
            else -> {
                throw InvalidNodeClass()
            }
        }
    }

    fun locate(p: Point): TrapezoidGraphLeaf? {
        val root = root ?: return null
        return locate(p, root)
    }

    private fun locate(p: Point, node: TrapezoidGraphNode): TrapezoidGraphLeaf? {
        when (node) {
            is TrapezoidGraphXNode -> {
                return when(p.compareX(node.point)) {
                    Point.LEFT -> locate(p, node.left)
                    Point.ON ->  locate(p, node.right)
                    Point.RIGHT -> locate(p, node.right)
                    else -> throw Exception()
                }
            }
            is TrapezoidGraphYNode -> {
                return when(node.segment.findSide(p)) {
                    LineSegment.SIDE.ABOVE -> locate(p, node.left)
                    LineSegment.SIDE.ON -> locate(p, node.right)
                    LineSegment.SIDE.BELOW -> locate(p, node.right)
                }
            }
            is TrapezoidGraphLeaf -> {
                return node
            }
            else -> {
                return null
            }
        }
    }

    class InvalidNodeClass : Exception()
}