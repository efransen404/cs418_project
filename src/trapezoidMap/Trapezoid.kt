package trapezoidMap

import data.LineSegment
import data.Point

class Trapezoid(
    var top: LineSegment,
    var bottom: LineSegment,
    var leftP: Point,
    var rightP: Point,
    var tAbove: Trapezoid?,
    var tBelow: Trapezoid?,
    var tLeft: Trapezoid?,
    var tRight: Trapezoid?) {
    lateinit var leaf: TrapezoidGraphLeaf
    override fun toString(): String {
        var result = ""
        result += top.toString() + "\n"
        result += bottom.toString() + "\n"
        result += leftP.toString() + "\n"
        result += rightP.toString() + "\n" + "\n"
        return result
    }
}
