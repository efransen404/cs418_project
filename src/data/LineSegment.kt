package data
data class Point(var x: Double = 0.0, var y: Double = 0.0) {

    /**
     * Will return [SIDE.LEFT] if other is to the left of this point,
     * or [SIDE.ON] if the other point is directly above/below this point, or
     * [SIDE.RIGHT] if the other point is to the right of this point
     */
    fun compareX(other: Point): SIDE {
        return (if(x != other.x) {
            when (x.compareTo(other.x)) {
                -1 -> LEFT
                1 -> RIGHT
                else -> throw Exception()
            }
        } else {
            when (y.compareTo(other.y)) {
                -1 -> LEFT
                0 -> ON
                1 -> RIGHT
                else -> throw Exception()
            }
        })
    }
    override fun toString(): String {
        return "($x, $y)"
    }
    enum class SIDE {
        LEFT, ON, RIGHT
    }
    companion object {
        val LEFT = SIDE.LEFT
        val ON = SIDE.ON
        val RIGHT = SIDE.RIGHT
    }

}

class LineSegment(start: Point, end: Point, val face: String) {
    val start: Point
    val end: Point
    val slope = (end.y - start.y) / (end.x - start.x)
    init {
        // I decided that lines will always go from left to right, so we'll switch them if we have to
        if(start.x > end.x) {
            this.start = end
            this.end = start
        } else {
            this.start = start
            this.end = end
        }
    }
    /**
     * Returns [ABOVE] when p is above the segment
     * If returns [ON], p is on the the line
     * If returns [BELOW], p is below the line segment
     */
    fun findSide(p: Point): SIDE {
        if(p.compareX(start) == Point.SIDE.LEFT || p.compareX(end) == Point.SIDE.RIGHT) { // We shouldn't be calling this function if the function is out of bounds
            throw OutOfBoundsException("Point is outside bounds of line segment\nThis: $start, $end\n Other: $p")
        }
        return if(start.x == end.x) { // If this is a vertical line, then the point is on it, because its within bounds
            ON
        } else {
            if((end.x - start.x) * (p.y - start.y) - (end.y - start.y) * (p.x - start.x) > 0) ABOVE else BELOW
        }
    }

    override fun toString(): String {
        return "($start, $end)"
    }
    enum class SIDE {
        ABOVE, ON, BELOW
    }
    companion object {
        val ABOVE = SIDE.ABOVE
        val ON = SIDE.ON
        val BELOW = SIDE.BELOW
    }
}

class OutOfBoundsException(message: String) : Exception(message) {
    constructor(): this("")
}