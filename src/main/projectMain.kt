package main

import DCEL.DoublyConnectedEdgeList
import data.LineSegment
import data.Point
import trapezoidMap.TrapezoidMap
import java.io.File
import java.io.FileNotFoundException
import java.io.InputStream

fun main() {
    var outerDone = false
    while(!outerDone) {
        System.out.print("Name of input segment file: ")
        val input = readLine()
        val reg = Regex("[.a-z0-9-]+")
        if(input == null) continue
        if(input == "x")
        {
            outerDone = true
        } else {
            val match = reg.matchEntire(input) ?: continue
            val filename = match.value
            var inputStream:InputStream? = null
            try {
                inputStream = File(filename).inputStream()
            } catch (e: FileNotFoundException) {
                println("File not found. Try again.")
            }
            if(inputStream == null) continue
            val source = inputStream.bufferedReader().use { it.readText() }
            val dcel = DoublyConnectedEdgeList(source)
            var segments: MutableList<LineSegment> = ArrayList()
            segments = dcel.getEdges().toMutableList()
            var trapezoidMap = TrapezoidMap(segments)
            System.out.println("Trapezoid map created successfully!")
            var trapezoidMapString = ""
            trapezoidMapString = trapezoidMap.toFileString()
            var outputFilePath = "trapezoidalMap.txt"
            File(outputFilePath).printWriter().append(trapezoidMapString).close()
            System.out.println("Trapezoid map saved successfully!")
            var innerDone = false
            while(!innerDone) {
                System.out.println("Query point: ")
                val regex2 = Regex("(\\([0-9]+.[0-9]+,[0-9]+.[0-9]+\\)|\\([0-9],[0-9]\\))")
                val input2 = readLine() ?: continue
                if(input2 == "x") {
                    innerDone = true
                } else {
                    val match2 = regex2.matchEntire(input2) ?: continue
                    val tokenized = match2.value.replace("(","").replace(")","").replace(","," ")
                    val split = tokenized.split(" ")
                    val x = split[0].toDouble()
                    val y = split[1].toDouble()
                    val p = Point(x, y)
                    val t = trapezoidMap.locate(p)
                    if(t == null)
                        System.out.println("null")
                    else {
                        println("Trapezoid contianing the point " + match2.value)
                        println(t.trapezoid.toString())
                        println()
                    }
                }
            }
        }

    }
}